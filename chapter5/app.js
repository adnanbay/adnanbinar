const http = require('http');

const hostname = '127.0.0.1';
const port = 8000;

const server = http.createServer((req, res) => {
  
    if (req.method === "GET") {
        res.statusCode = 200;
        res.end("Method GET");    
    } else if (req.method === "POST"){
        res.statusCode = 200;
        res.end("Method POST");
    } else {
        res.statusCode = 200;
        res.end('Method Selain GET dan POST');
    }    

});

server.listen(port, hostname, () => {
  console.log(`Server running at http://${hostname}:${port}/`);
});