const userList = [];
const md5 = require("md5");
const { Op } = require("sequelize");

const db = require('../models');

class UserModel{
    // Dapat semua data
    getAllUsers = async () => {
        const dataUsers = await db.User.findAll({ include: [db.UserBio, db.UserGameHistory]});
        // SELECT * FROM USERS
        return dataUsers;
    };

    getSingleUser = async (idUser) => {
        return await db.User.findOne({ 
            include: [db.UserBio],
            where: { id : idUser},
        });
    };

    // Method cek user bio
    isUserBioExist = async (userBio) => {
        const bioExist = await db.UserBio.findOne({
            where: {
                [Op.or]: [
                    {fullname: userBio.fullname},
                    {phoneNumber: userBio.phoneNumber},
                    {user_id: userBio.user_id},
                ],
            },
        });
        
        if (bioExist) {
            return true;
        } else {
            return false;
        }
    };
    
    updateUserBio = async (idUser, fullname, address, phoneNumber, ig, twitter)=> {
        return await db.UserBio.update(
        { fullname: fullname, address: address, phoneNumber:phoneNumber, ig:ig, twitter:twitter},
        { where: { user_id : idUser}}
        );
    }

    // Method data sudah teregistrasi atau belum
    isUserRegistered = async (dataReq) => {
        const dataAda = await db.User.findOne ({
            where: { [Op.or] : [
                { username : dataReq.username},
                { email: dataReq.email},
            ],
            },
        });
        
        // const dataAda = userList.find((data) => {
        // return (
        // data.username === dataReq.username || 
        // data.email === dataReq.email
        // );
        // });
        if (dataAda){
            return true;
        } else {
            return false;
        };
    
    };

    // Method tambahin user bio
    recordNewUserBio = (userBio) => {
        db.UserBio.create({
            fullname: userBio.fullname,
            address: userBio.address,
            phoneNumber: userBio.phoneNumber,
            ig: userBio.ig,
            twitter: userBio.twitter,
            user_id: userBio.user_id
        });
    }
    
    // Method record data
    recordNewData = (dataReq) => {
    // INSERT INTO table () values () 
    db.User.create({
    username: dataReq.username,
    email: dataReq.email,
    password: md5(dataReq.password),
     });
       
    };

    // Method input game history
    recordGame = (dataGame) => {
        db.UserGameHistory.create({
            user_id: dataGame.user_id,
            status: dataGame.status
        });
    };

    gameHistory = async (idUser) => {
        return await db.User.findOne({ 
            include: [db.UserGameHistory],
            where: { id : idUser}});
    }
    
    // Method Login
    verifyLogin = async (username, password) => {
    //     const sortedValue = userList.find((user) => {
    //     return user.username === username  && user.password === md5(password);
    // });

        const sortedValue = await db.User.findOne ({
            where: { username:username, password: md5(password)},
        });
        
        return sortedValue;
    }
    

}

module.exports = new UserModel();